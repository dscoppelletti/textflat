# Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import string
from collections import deque

class CharFilter:
    OP_YIELD = 0
    OP_EXEC = 1

    def __init__(self, source):
        self._source = source
        self._queue = deque()

    def __iter__(self):
        for c in self._source:
            self.consume(c)
            while len(self._queue) > 0:
                op = self._queue.popleft()
                if op.operator == CharFilter.OP_YIELD:
                    yield op.operand
                elif op.operator == CharFilter.OP_EXEC:
                    op.operand()

    def consume(self, c):
        pass

    def add_operation(self, operator, operand):
        self._queue.append(Operation(operator, operand))

    def is_whitespace(self, c):
        return c in string.whitespace or ord(c) == 160

class Operation:

    def __init__(self, operator, operand):
        self.operator = operator
        self.operand = operand