# Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

class CharReplace:

    def __init__(self, source, charmap):
        self._source = source
        self._charmap = charmap

    def __iter__(self):
        for u in self._source:
            key = format(ord(u), "04X")
            s = self._charmap.get(key, unicode(u))
            for c in s:
                yield c