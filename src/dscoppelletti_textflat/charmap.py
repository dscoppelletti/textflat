# Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json

# http://stackoverflow.com/questions/663058 - March 19, 2009
from HTMLParser import HTMLParser

class CharMap(dict):

    def __init__(self, name):
        super(CharMap, self).__init__()

        with open(name, 'r') as f:
            d = json.load(f)
        self._parse(d)

    def _parse(self, d):
        h = HTMLParser()
        for (key, value) in d.items():
            self[key] = self._parse_value(value, h)

    def _parse_value(self, value, h):
        if isinstance(value, dict):
            if value.get('xmlencoded', False):
                return h.unescape(value['value'])
            else:
                return value['value']
        else:
            return value

