# Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from charfilter import CharFilter

class WordWrap(CharFilter):

    def __init__(self, source, linelen_max, keep_newline):
        CharFilter.__init__(self, source)
        self._linelen_max = linelen_max
        self._keep_nl = keep_newline
        self._lastchar_cr = False
        self._line = []

    def __iter__(self):
        for c in CharFilter.__iter__(self):
            yield c
        if len(self._line) > 0:
            for c in self._line:
                yield c
            yield "\n"

    def consume(self, c):
        if c == "\n":
            if self._lastchar_cr:
                # No NL after CR
                self._lastchar_cr = False
            elif self._keep_nl:
                self._flushline()
            else:
                self._add_whitespace()
            return

        if c == "\r":
            if self._keep_nl:
                self._flushline()
            else:
                self._add_whitespace()

            # Maybe there will be NL after CR
            self._lastchar_cr = True
            return

        self._lastchar_cr = False
        if self.is_whitespace(c):
            self._add_whitespace()
        else:
            self._add_char(c)

    def _add_char(self, c):
        if len(self._line) < self._linelen_max:
            # Enough space in current line
            self._line.append(c)
            return

        # Current line exhausted:
        # Search for the last whitespace where I can break the line.
        i = len(self._line) - 1
        k = -1
        while k < 0 <= i:
            if self._line[i] == ' ':
                k = i
            else:
                i -= 1

        if k < 0:
            # No whitespace found:
            # Break the line anyway.
            self._flushline()
            self._line.append(c)
            return

        # Flush the line until the last whitespace
        for x in self._line[0:k]:
            self.add_operation(CharFilter.OP_YIELD, x)
        self.add_operation(CharFilter.OP_YIELD, "\n")

        # Append the character to the remainder of the line
        del self._line[0:(k + 1)]
        self._line.append(c)

    def _add_whitespace(self):
        n = len(self._line)
        if n < self._linelen_max:
            # Enough space in current line
            if n > 0:
                self._line.append(' ')
        else:
            # Current line exhausted:
            # Replace whitespace by NL.
            self._flushline()

    def _flushline(self):
        for c in self._line:
            self.add_operation(CharFilter.OP_YIELD, c)
        self.add_operation(CharFilter.OP_YIELD, "\n")
        self._line = []
