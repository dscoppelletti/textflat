# Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from charfilter import CharFilter

class WhitespaceCollapse(CharFilter):

    def __init__(self, source):
        CharFilter.__init__(self, source)
        self._linelen = 0
        self._lastchar_cr = False
        self._lastchar_whitespace = True
        self._lastline_empty = True
        self._newline_pending = False
        self._whitespace_pending = False

    def consume(self, c):
        if c == "\n":
            if self._lastchar_cr:
                # No NL after CR
                self._lastchar_cr = False
            else:
                self._newline()
            return

        if c == "\r":
            self._newline()

            # Maybe there will be NL after CR
            self.add_operation(CharFilter.OP_EXEC, self._after_cr)
            return

        if self.is_whitespace(c):
            # No whitespace at the start of line
            if not self._lastchar_whitespace:
                self._whitespace_pending = True
            return

        if self._newline_pending:
            self._do_newline()

        if self._whitespace_pending:
            self.add_operation(CharFilter.OP_YIELD, ' ')
            self.add_operation(CharFilter.OP_EXEC, self._after_whitespace)

        self.add_operation(CharFilter.OP_YIELD, c)
        self.add_operation(CharFilter.OP_EXEC, self._after_char)

    def _newline(self):
        if self._linelen == 0:
            # Will not insert NL after another NL
            if not self._lastline_empty:
                self._newline_pending = True
        else:
            self._do_newline()

    def _do_newline(self):
        self.add_operation(CharFilter.OP_YIELD, "\n")
        self.add_operation(CharFilter.OP_EXEC, self._after_nl)

    def _after_char(self):
        self._linelen += 1
        self._lastchar_cr = False
        self._lastchar_whitespace = False

    def _after_whitespace(self):
        self._linelen += 1
        self._lastchar_whitespace = True
        self._whitespace_pending = False

    def _after_nl(self):
        self._lastchar_cr = False
        self._lastchar_whitespace = True
        self._lastline_empty = (self._linelen == 0)
        self._linelen = 0
        self._newline_pending = False
        self._whitespace_pending = False

    def _after_cr(self):
        self._lastchar_cr = True


